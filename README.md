# Work Item Field Syntax Save Hook

The latest jar file is available in the [releases section](../../releases).

## Introduction

Are you tired of your users not following syntax rules or basic logic for the different fields of work items?
- Ignoring CamelCase snake_case or similar rules?
- Using titles that are too long or too short?
- Using "forbidden" words in the description?
- Entering infeasible values for integer, float or currency fields?
- Specifying start or due dates that are out of the project period?

Then this extension is the solution for you. For almost every field type, you can specify a regular expression (RegEx) that the content has to fulfill. If at least one field does not fulfill the RegEx, the user won't be able to save the work item but will be displayed an optional error message. That way they can instantly correct their mistake.

This work item save hook supports the following field types:
- Text (rich and plain text will be converted to plain text)
- String
- Duration (rendered as e.g. `5d 2h` or `42h` depending on how the user enters the data)
- Date (rendered as `yyyy-MM-dd`)
- Date time (rendered as `yyyy-MM-dd HH:mm`)
- Integer
- Float
- Currency (rendered with two decimal digits e.g. `22.00`)
- Time (rendered as `HH:mm:ss.SSS`)

Their content will be converted to a string and evaluated against the regular expression.
Per rule, you can specify an error message that will be displayed if the RegEx is not fulfilled. If no error message is specified, a default message containing the RegEx will be displayed.

## License

Download and use of the tool is free. By downloading the tool, you accept the license terms.

See [NOTICE.md](NOTICE.md) for licensing details.


## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for contribution guidelines.


## Download & Installation

### Installation:

1. Have the Work Item Action Interceptor Framework (see [Installation Requirements](#Installation-Requirements)) installed and follow the instructions of that extension.

1. Go to the [releases section](../../releases) of this repository and download the latest version of the hook.

1. Copy the jar file in the `hooks` folder of the interceptor framework on your Polarion server.

1. Add the [configuration properties](#Configuration-Properties) in the `hooks.properties` file of the interceptor framework.

1. Follow the instructions of the interceptor framework on how to reload the hooks and configuration properties.

### Configuration Properties:

This hook has the following syntax for the configuration properties:

`FieldSyntax.<Project ID>.<WorkItem Type ID>.<Field ID>.regex=<the regular expression>`  and 

`FieldSyntax.<Project ID>.<WorkItem Type ID>.<Field ID>.message=<the optional error message>` where

- `<Project ID>` and `<WorkItem Type ID>` can be replaced by a `*` so that the rule applies for all projects and/or work item types. More specific rules override more generic rules: `Project.WorkItem > Project.* > *.WorkItem > *.*`

- `<Field ID>` is the id of the built-in field or a custom field. The field must be of a type specified in the list at the top. If the field is of a not supported type, the rule is ignored.

#### Syntax Example:

`FieldSyntax.DummyProject.aircraft_configuration.center_of_gravity.regex=^(-?[0-9]{1,6}(?:\\.[0-9]{1,6})?(?::-?[0-9]{1,6}(?:\\.[0-9]{1,6})?)?(?::-?[0-9]{1,6}(?:\\.[0-9]{1,6})?)?|-?[0-9]{1,6}(?:\\.[0-9]{1,6})?(?:, ?-?[0-9]{1,6}(?:\\.[0-9]{1,6})?)*)$`

`FieldSyntax.DummyProject.aircraft_configuration.center_of_gravity.message=Syntax start_value:stepsize:end_value or comma separated values with all digits not met. (stepsize and end_value are optional, maximum 6 digits before decimal, maximum 6 digits after decimal, negative values are allowed)`

### Uninstall:

- Remove the jar file from the `hooks` folder of the interceptor framework, remove the corresponding entries in the `hooks.properties` file and reload the hooks and properties in the `Action Interceptor Manager` wiki page.

### Installation Requirements:

- [Work Item Action Interceptor Framework](https://extensions.polarion.com/extensions/146-work-item-action-interceptor-framework) version 3.0.3 and later

- Polarion version 3.19.1 and later