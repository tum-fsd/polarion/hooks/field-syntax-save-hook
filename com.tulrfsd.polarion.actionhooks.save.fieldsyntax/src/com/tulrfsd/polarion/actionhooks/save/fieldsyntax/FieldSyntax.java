package com.tulrfsd.polarion.actionhooks.save.fieldsyntax;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import com.adva.polarion.actionhook.IActionHook;
import com.polarion.alm.tracker.model.IPlan;
import com.polarion.alm.tracker.model.ITestRun;
import com.polarion.alm.tracker.model.IWorkItem;
import com.polarion.core.util.logging.Logger;

public class FieldSyntax implements IActionHook {

  public static final String HOOK_ID = "FieldSyntax";
  public static final String HOOK_DESCRIPTION =
      "Checks syntax of fields against a regular expression";
  private static final String HOOK_VERSION = "1.1.1";
  
  private static Logger logger = Logger.getLogger(FieldSyntax.class.getName());

  private HashMap<String, String> hookProperties = new HashMap<>();
  
  private HashMap<String, HashMap<String, String>> regExMap = new HashMap<>();
  private HashMap<String, HashMap<String, String>> messageMap = new HashMap<>();
  
  private HashSet<String> workItemBlacklist = new HashSet<>();

  @Override
  public ACTION_TYPE getAction() {
    return ACTION_TYPE.SAVE;
  }

  @Override
  public String getDescription() {
    return HOOK_DESCRIPTION;
  }

  @Override
  public String getName() {
    return HOOK_ID;
  }

  @Override
  public String getVersion() {
    return HOOK_VERSION;
  }

  @Override
  public void init() {
    // no hook init required
  }

  @Override
  public void initProperties(Properties properties) {
    
    this.workItemBlacklist.clear();
    this.regExMap.clear();
    this.messageMap.clear();
    this.hookProperties.clear();

    if (properties == null) {
      return;
    }
    
    // read all properties from the global file belonging to this hook
    for (String propertyKey : properties.stringPropertyNames()) {
      if (propertyKey.startsWith(this.getName() + ".") && !properties.getProperty(propertyKey).isEmpty()) {
        this.hookProperties.put(propertyKey.replace(this.getName() + ".", ""), properties.getProperty(propertyKey));
      }
    }
  }

  @Override
  public boolean isEnabled() {
 // deprecated in interface, controlled by properties file
    return true;
  }

  @Override
  public String processAction(IWorkItem workItem) {
    
    // try/catch to make sure that Polarion can still be used even if something goes wrong with this hook
    try {
      
      String projectAndWorkItemTypeId = workItem.getProjectId() + "." + workItem.getType().getId();
      
      // check if combination of project and work item type is already on blacklist
      if (workItemBlacklist.contains(projectAndWorkItemTypeId)){
        return null;
      }
    
      // generate maps with field:RegEx and field:Message if specific maps do not exist yet
      if (!regExMap.containsKey(projectAndWorkItemTypeId)) {
        HashMap<String, String> fieldRegExMap = new HashMap<>();
        HashMap<String, String> fieldMessageMap = new HashMap<>();
        
        // first look for specific work item type in specific project
        putRegExAndMessageForFields(fieldRegExMap, fieldMessageMap, projectAndWorkItemTypeId);
        // second look for generic work item type in specific project
        putRegExAndMessageForFields(fieldRegExMap, fieldMessageMap, workItem.getProjectId() + ".*");
        // third look for specific work item type in generic project
        putRegExAndMessageForFields(fieldRegExMap, fieldMessageMap, "*." + workItem.getType().getId());
        // fourth look for generic work item type in generic project
        putRegExAndMessageForFields(fieldRegExMap, fieldMessageMap, "*.*");
        
        if (fieldRegExMap.isEmpty()) {
          // put combination of project and work item type on blacklist because properties file does not contain config for them
          workItemBlacklist.add(projectAndWorkItemTypeId);
        } else {
          // add field:RegEx and Message map to overall maps
          regExMap.put(projectAndWorkItemTypeId, fieldRegExMap);
          messageMap.put(projectAndWorkItemTypeId, fieldMessageMap);
        }
      }
      
      // check again if current work item is on blacklist, if not perform syntax checks
      if (workItemBlacklist.contains(projectAndWorkItemTypeId)) {
        return null;
      }
      // perform check
      return performSyntaxCheck(workItem, regExMap.get(projectAndWorkItemTypeId), messageMap.get(projectAndWorkItemTypeId));
    
    } catch (Exception e) {
      // write exception in log
      logger.error("While processing work item " + workItem.getId() + ": " + e);
    }

    return null;
  }

  @Override
  public String processAction(IPlan plan) {
    // hook not configured for plans
    return null;
  }

  @Override
  public String processAction(ITestRun testRun) {
    // hook not configured for testRuns
    return null;
  }


  @Override
  public void setEnabled(boolean arg0) {
    // deprecated in interface
  }
  
  
  // put the regular expression and the error message in maps specific for the combination of project and work item type
  private void putRegExAndMessageForFields(HashMap<String, String> fieldRegExMap, HashMap<String, String> fieldMessageMap, String key) {
    for (Map.Entry<String, String> entry : hookProperties.entrySet()) {
      
      String propertyKey = entry.getKey();
      String regEx = entry.getValue();
      
      if (propertyKey.startsWith(key + ".") && propertyKey.endsWith(".regex")) {
        fieldRegExMap.putIfAbsent(propertyKey.replace(key + ".", "").replace(".regex", ""), regEx);
      }
      if (propertyKey.startsWith(key + ".") && propertyKey.endsWith(".message")) {
        fieldMessageMap.putIfAbsent(propertyKey.replace(key + ".", "").replace(".message", ""), regEx);
      }
    }
  }
  
  
  private String performSyntaxCheck(IWorkItem workItem, HashMap<String, String> fieldRegExMap, HashMap<String, String> fieldMessageMap) {
    
    boolean checkPassed = true;
    
    for (Map.Entry<String, String> entry : fieldRegExMap.entrySet()) {
      
      String fieldKey = entry.getKey();
      String regEx = entry.getValue();
      
      checkPassed = HookUtils.checkFieldSyntax(workItem, fieldKey, regEx);
      
      if (!checkPassed) {
        // display message instead of regEx when message is available
        if (fieldMessageMap.containsKey(fieldKey)) {
          return "Syntax error for field " + workItem.getFieldLabel(fieldKey) + ":\n" + fieldMessageMap.get(fieldKey);
        }
        return "The field " + workItem.getFieldLabel(fieldKey) + " does not match the RegEx " + regEx;
      }
    }
    return null;
    
  }

}
