package com.tulrfsd.polarion.actionhooks.save.fieldsyntax;

import java.text.SimpleDateFormat;
import java.util.Date;
import com.polarion.alm.tracker.model.IWorkItem;
import com.polarion.core.util.types.Currency;
import com.polarion.core.util.types.DateOnly;
import com.polarion.core.util.types.Text;
import com.polarion.core.util.types.TimeOnly;
import com.polarion.core.util.types.duration.DurationTime;
import com.polarion.subterra.base.data.model.IPrimitiveType;
import com.polarion.subterra.base.data.model.IType;

public interface HookUtils {
  
  public static boolean isFieldDefined(IWorkItem workItem, String fieldId) {
    
    return workItem.getPrototype().getKeyNames().contains(fieldId) || workItem.getCustomFieldsList().contains(fieldId);
  }
  
  public static String getFieldTypeName(IWorkItem workItem, String fieldId) {
    
    if (!isFieldDefined(workItem, fieldId)) {
      return "";
    }
    
    IType fieldType = workItem.getFieldType(fieldId);
    
    // currently, only primitive field types are supported
    if (!(fieldType instanceof IPrimitiveType)) {
      return "";
    }
    String fieldTypeName = ((IPrimitiveType) fieldType).getTypeName();
    switch (fieldTypeName) {
      // list of supported primitive field types
      case "com.polarion.core.util.types.Text":
      case "com.polarion.core.util.types.duration.DurationTime":
      case "com.polarion.core.util.types.DateOnly":
      case "java.util.Date":
      case "java.lang.Integer":
      case "java.lang.Float":
      case "com.polarion.core.util.types.Currency":
      case "com.polarion.core.util.types.TimeOnly":
      case "java.lang.String":
        return fieldTypeName;
      default:
        return "";
    }
  }
  
  public static Boolean checkFieldSyntax(IWorkItem workItem, String fieldKey, String regEx) {
    
    Object field = workItem.getValue(fieldKey);
    if (field == null) {
      return true;
    }

    switch (getFieldTypeName(workItem, fieldKey)) {
      case "com.polarion.core.util.types.Text":
        return checkTextFieldSyntax((Text) field, regEx);
      case "com.polarion.core.util.types.duration.DurationTime":
        return checkDurationTimeFieldSyntax((DurationTime) field, regEx);
      case "com.polarion.core.util.types.DateOnly":
        return checkDateFieldSyntax((DateOnly) field, regEx);
      case "java.util.Date":
        return checkDateTimeFieldSyntax((Date) field, regEx);
      case "java.lang.Integer":
        return checkIntegerFieldSyntax((int) field, regEx);
      case "java.lang.Float":
        return checkFloatFieldSyntax((float) field, regEx);
      case "com.polarion.core.util.types.Currency":
        return checkCurrencyFieldSyntax((Currency) field, regEx);
      case "com.polarion.core.util.types.TimeOnly":
        return checkTimeFieldSyntax((TimeOnly) field, regEx);
      case "java.lang.String":
        return checkStringFieldSyntax((String) field, regEx);
      default:
        return true;
    }
    
  }
  
  
  public static boolean checkStringFieldSyntax(String string, String regEx) {
    return string.matches(regEx);
  }
  
  public static boolean checkIntegerFieldSyntax(int number, String regEx) {
    return checkStringFieldSyntax(Integer.toString(number), regEx);
  }
  
  public static boolean checkFloatFieldSyntax(float number, String regEx) {
    return checkStringFieldSyntax(Float.toString(number), regEx);
  }
  
  public static boolean checkCurrencyFieldSyntax(Currency amount, String regEx) {
    return checkStringFieldSyntax(amount.toString(), regEx);
  }
  
  public static boolean checkTimeFieldSyntax(TimeOnly time, String regEx) {
    return checkStringFieldSyntax(time.toString().split(" ")[0], regEx);
  }
  
  public static boolean checkDateFieldSyntax(DateOnly dateOnly, String regEx) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    return checkStringFieldSyntax(dateFormat.format(dateOnly.getDate()), regEx);
  }
  
  public static boolean checkDurationTimeFieldSyntax(DurationTime duration, String regEx) {
    return checkStringFieldSyntax(duration.toString(), regEx);
  }
  
  public static boolean checkDateTimeFieldSyntax(Date date, String regEx) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    return checkStringFieldSyntax(dateFormat.format(date), regEx);
  }
  
  public static boolean checkTextFieldSyntax(Text text, String regEx) {
    return checkStringFieldSyntax(text.convertToPlainText().toString().replace("[Lossy]text/plain:\n", "").replace("text/plain:\n", ""), regEx);
  }
}